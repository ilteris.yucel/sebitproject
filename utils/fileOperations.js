import { downloadFile, stat, DocumentDirectoryPath } from 'react-native-fs';
import { getFreeDiskStorage } from 'react-native-device-info';
const remoteFileSize = require('remote-file-size');

const memoryCheck = async (url) => {
  console.log(url);
  const fileSize = await stat(url);
  console.log('File size: ' + fileSize);
  const availableMemory = await getFreeDiskStorage();
  console.log('Available Memory: ' + availableMemory);
  return availableMemory >= fileSize;
}

const downloadPDF = async (url, filename) => {
  console.log(url);
  console.log(filename);
  const path = `${DocumentDirectoryPath}/${filename}`;
  const headers = {
    'Accept': 'application/pdf',
    'Content-Type': 'application/pdf'
  }
  const options = {
    fromUrl: url,
    toFile: path,
    headers: headers
  }

  downloadFile(options).promise.then((res) => {
    if(res && res.statusCode === 200 && res.bytesWritten > 0){
        console.log('File download succesfull');
        return true;
      }
    else{
      ret = false;
      console.log('File download unseccesfull');
      return false;
    }
  })
}

export { memoryCheck, downloadPDF };