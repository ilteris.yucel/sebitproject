import { combineReducers } from 'redux';
const initialState = {file : ''}
const pdfReducer = ( state = initialState, action) => {
  switch(action.type){
    case 'SET_PDF':
      console.log("SETTING PDF " + action.payload);
      const newState = { ...state, file: action.payload };
      console.log(newState);
      return newState;
    default:
      return state;
  } 
};
export default combineReducers({
  pdf: pdfReducer
});