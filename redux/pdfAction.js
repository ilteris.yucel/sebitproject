export const setPDF = url => ({
  type: 'SET_PDF',
  payload: url
});