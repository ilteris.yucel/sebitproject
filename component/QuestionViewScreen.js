import React, { Component } from 'react';
import { mainStyles } from '../styles/mainStyles';
import WebView from "react-native-webview";
import PagerView from 'react-native-pager-view';
import { Button, View } from 'react-native';


class QuestionViewScreen extends Component{
  constructor(props){
    super(props);
    this.state = {
      visible: false,
      questions: []
    }
  }
  componentDidMount(){
    this.setState({visible:true})

  }
  createQuestions(){
    let questionList = []
    this.props.route.params.questions.forEach((q) => {
      let comp = <WebView>
        originWhitelist={['*']}
        source={q}
      </WebView>;
       questionList.push(comp);
    });
    console.log(questionList);
    return questionList;
  }

  render(){

    return(
      <PagerView style={{flex:1}}>
        {this.createQuestions()}
      </PagerView>

      
    )
  }
}


export default QuestionViewScreen;