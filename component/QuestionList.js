import React, { Component } from 'react';
import { mainStyles } from '../styles/mainStyles';
import { Text, View, FlatList } from 'react-native';
import  { setPDF } from '../redux/pdfAction';
import { connect } from 'react-redux';
import QuestionItem from './QuestionItem';

class QuestionList extends Component{
  constructor(props){
    super(props);
    this.state = {

    }
  }

  componentDidMount(){

}    
  render(){
    return(
        <FlatList
          data={this.props.data}
          renderItem={({item}) => <QuestionItem navigation={this.props.navigation} route={this.props.route} questions={item.questions} title={item.title}></QuestionItem>}
          keyExtractor={(item) => item.title}
          ListHeaderComponent={() => <Text style={mainStyles.sectionTitle}>Soru Listesi</Text>}
        >
  
        </FlatList>
    );
  }
}

export default QuestionList;