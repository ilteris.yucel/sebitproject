import React, { Component } from 'react';
import { mainStyles } from '../styles/mainStyles';
import ImageView from "react-native-image-viewing";
import { Button, View } from 'react-native';


class GalleryViewScreen extends Component{
  constructor(props){
    super(props);
    this.state = {
      visible: false,
      images: []
    }
  }
  componentDidMount(){
    this.setState({visible:true})

  }
  createQestions(){
    
  }

  render(){
    let imageList = []
    this.props.route.params.images.forEach((img) => {
      let obj = {uri: null};
      obj.uri = img.original;
      imageList.push(obj);
    })
    return(
      <View>
        <ImageView
          images={imageList}
          imageIndex={0}
          visible={this.state.visible}
          onRequestClose={() => this.setState({visible:false})}
        >
        </ImageView>
        
      </View>

      
    )
  }
}


export default GalleryViewScreen;