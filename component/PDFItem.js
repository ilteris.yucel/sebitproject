import React, { Component } from 'react';
import { mainStyles } from '../styles/mainStyles';
import { Text, View } from 'react-native';
import { downloadFile , DocumentDirectoryPath} from 'react-native-fs';
import { bindActionCreators } from 'redux';
import  { setPDF } from '../redux/pdfAction';
import { connect } from 'react-redux';

class PDFItem extends Component{
  constructor(props){
    super(props);
    this.onTextPress = this.onTextPress.bind(this);
    this.state = {

    };
    this.filename = null;
  }
  componentDidMount(){
    const arr = this.props.fileUrl.split('/');
    this.filename = arr[arr.length - 1];
  }
  async downloadPDF(url, filename){
    console.log(url);
    console.log(filename);
    const path = `${DocumentDirectoryPath}/${filename}`;
    const headers = {
      'Accept': 'application/pdf',
      'Content-Type': 'application/pdf'
    }
    const options = {
      fromUrl: url,
      toFile: path,
      headers: headers
    }
  
    downloadFile(options).promise.then((res) => {
      if(res && res.statusCode === 200 && res.bytesWritten > 0){
          this.props.setPDF(path);
          console.log('File download succesfull');
          this.props.navigation.navigate('PDFView');
        }
      else{
        console.log('File download unseccesfull');
      }
    })
  }
  async onTextPress(event){
    console.log("OK");
    await this.downloadPDF(this.props.fileUrl, this.filename);
  }

  render(){

    return(
    <View>
      <Text 
        onPress={async (e) => await this.onTextPress(e)}
        style={mainStyles.sectionDescription}
      >
        {this.props.title}
      </Text>
    </View>

    );
  }
}

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    setPDF,
  }, dispatch)
);


export default connect(null, mapDispatchToProps)(PDFItem);
