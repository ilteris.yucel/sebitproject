import React, { Component } from 'react';
import { mainStyles } from '../styles/mainStyles';
import { Text, View } from 'react-native';
import { connect } from 'react-redux';
import Pdf from 'react-native-pdf';



class PDFViewScreen extends Component{
  constructor(props){
    super(props);
    this.state = {

    }
    this.pdf = null;
  }
  componentDidMount(){

  }

  render(){
    console.log(this.props.file);
    const pdfUri = {uri:'file://' +this.props.file, cache:true}
    return(
      <View style={{flex:1}}>
        <Pdf
          ref={(pdf)=>{this.pdf = pdf;}}
          source={pdfUri}
          style={{flex: 1}}
          onError={(error)=>{console.log(error);}}
        />
      </View>

    )
  }
}


const mapStateToProps = (state) => {
  
  const { file } = state.pdf;
  return { file }
}
export default connect(mapStateToProps)(PDFViewScreen);
