import React, { Component } from 'react';
import { mainStyles } from '../styles/mainStyles';
import { Text, View, FlatList } from 'react-native';
import  { setPDF } from '../redux/pdfAction';
import { connect } from 'react-redux';
import GalleryItem from './GalleryItem';

class GalleryList extends Component{
  constructor(props){
    super(props);
    this.state = {

    }
  }

  componentDidMount(){

}    
  render(){
    return(
        <FlatList
          data={this.props.data}
          renderItem={({item}) => <GalleryItem navigation={this.props.navigation} route={this.props.route} images={item.images} title={item.title}></GalleryItem>}
          keyExtractor={(item) => item.title}
          ListHeaderComponent={() => <Text style={mainStyles.sectionTitle}>Galeri Listesi</Text>}
        >
  
        </FlatList>
    );
  }
}

export default GalleryList;