import React, { Component } from 'react';
import { mainStyles } from '../styles/mainStyles';
import { Text, View } from 'react-native';

class QeustionItem extends Component{
  constructor(props){
    super(props);
    this.state = {

    }
  }
  componentDidMount(){

  }
  onTextPress(event){
    this.props.navigation.navigate({name:'QuestionView', params:{questions:this.props.questions}});
  }
  render(){
    return(
      <View>
      <Text 
        onPress={async (e) => this.onTextPress(e)}
        style={mainStyles.sectionDescription}
      >
        {this.props.title}
      </Text>
    </View>
    )
  }
}

export default QeustionItem;