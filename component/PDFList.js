import React, { Component } from 'react';
import { Text, View , FlatList } from 'react-native';
import PDFItem from './PDFItem';
import { mainStyles } from '../styles/mainStyles';

class PDFList extends Component{
  constructor(props){
    super(props);
    this.state = {

    }
  }
  componentDidMount(){

  }
  render(){
    return(
        <FlatList
          data={this.props.data}
          renderItem={({item}) => <PDFItem navigation={this.props.navigation} fileUrl={item.fileUrl} title={item.title}></PDFItem>}
          keyExtractor={(item) => item.title}
          ListHeaderComponent={() => <Text style={mainStyles.sectionTitle}>PDF Listesi</Text>}
        >

        </FlatList>
    );
  }
}

export default PDFList;