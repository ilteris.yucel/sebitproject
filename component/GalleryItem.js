import React, { Component } from 'react';
import { mainStyles } from '../styles/mainStyles';
import { Text, View } from 'react-native';

class GalleryItem extends Component{
  constructor(props){
    super(props);
    this.onTextPress = this.onTextPress.bind(this);
    this.state = {
      visible:false
    }
  }
  componentDidMount(){
    this.setState({visible:true})
  }
  onTextPress(event){
    this.props.navigation.navigate({name:'GalleryView', params:{images:this.props.images}});
  }
  render(){
    return(
      <View>
        <Text 
          onPress={async (e) => this.onTextPress(e)}
          style={mainStyles.sectionDescription}
        >
          {this.props.title}
        </Text>
      </View>
    );
  }
}

export default GalleryItem;