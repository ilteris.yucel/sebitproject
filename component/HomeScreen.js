import React, { Component } from 'react';
import {View} from 'react-native';
import PDFList from './PDFList';
import GalleryList from './GalleryList';
import QuestionList from './QuestionList';

class HomeScreen extends Component{
  constructor(props){
    super(props);
    this.state = {
      fetchSucces: null,
      data: []
    }
  }

  componentDidMount(){
    fetch('https://sebit.s3.eu-central-1.amazonaws.com/androiddata.json')
      .then((response) => {
        if(response.status === 200){
          this.setState({fetchSucces:true});
          response.json()
          .then((json) => {
            this.setState({data : json});
          })
          .catch((error) => {
            console.log('Error on parsing data!');
            this.setState({fetchSucces:false});
          })
        } 
      })
      .catch((error) => {
        console.log('Error on fetching data!')
        this.setState({fetchSucces:false});
      })    
  }

  render(){
    console.log(this.props.navigation);
    console.log(this.props.route);
    const pdfList = this.state.data.filter((d) => {
      let ret = false;
      if(d.contentType === 'pdf'){
        ret = true;
      }
      return ret;
    });
    console.log(pdfList);

    const galleryList = this.state.data.filter((d) => {
      let ret = false;
      if(d.contentType === 'gallery'){
        ret = true;
      }
      return ret;
    });

    const questionList = this.state.data.filter((d) => {
      let ret = false;
      if(d.contentType === 'question'){
        ret = true;
      }
      return ret;
    });

    console.log(pdfList);
    return(
      <View>
        <PDFList navigation={this.props.navigation} data={pdfList}></PDFList>
        <GalleryList navigation={this.props.navigation} route={this.props.route} data={galleryList}></GalleryList>
        <QuestionList navigation={this.props.navigation} route={this.props.route} data={questionList}></QuestionList>
      </View>
    )
  }
}

export default HomeScreen;