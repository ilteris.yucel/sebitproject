/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component }  from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import pdfReducer from './redux/pdfReducer';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {
  SafeAreaView,
  StatusBar,
  Appearance,
  View,
} from 'react-native';
import HomeScreen from './component/HomeScreen';
import PDFViewScreen from './component/PDFViewScreen';
import GallerViewScreen from './component/GalleryViewScreen';
import QuestionViewScreen from './component/QuestionViewScreen';

import {
  Colors,
  
} from 'react-native/Libraries/NewAppScreen';



const store = createStore(pdfReducer);
const Stack = createNativeStackNavigator();

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      isDarkMode : false,
      backgroundStyle : {
        backgroundColor : Colors.lighter
      },
      fetchSucces: null,
      data: []
    }
    this.jsonData = null;
  }
  componentDidMount(){
    const colorScheme = Appearance.getColorScheme();
    if(colorScheme === 'dark'){
      this.setState({isDarkMode : true});
      this.setState({backgroundStyle : {backgroundColor : Colors.darker}});
    }
  }
  render(){
    return (
      <Provider store={store}>
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name='HOME' component={HomeScreen}/>
            <Stack.Screen name='PDFView' component={PDFViewScreen}/>
            <Stack.Screen name='GalleryView' component={GallerViewScreen}/>
            <Stack.Screen name='QuestionView' component={QuestionViewScreen}/>
          </Stack.Navigator>
        </NavigationContainer>   
      </Provider>

    );
  }

};


export default App;
